(function(){
    
    "use strict";

    //récupération du boutton qui permet de revenir au début de la page
    var scroll = document.querySelector('.scrollTop');

     //déclenche un retour au début de la page lors d'un click sur le bouton
    scroll.addEventListener('click',function()
    {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    });
    
    //cache ou fait apparaitre le bouton en fonction de la position sur la page
    document.addEventListener('scroll',function(event)
    {
        if (window.scrollY >= 100) {
            scroll.classList.remove('hide');
        } else {
            scroll.classList.add('hide');
        }
    });

})();
